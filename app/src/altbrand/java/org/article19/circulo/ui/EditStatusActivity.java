package org.article19.circulo.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.article19.circulo.FragmentMain;
import org.article19.circulo.adapter.StatusUpdatesRecyclerViewAdapter;
import org.article19.circulo.adapter.TagsRecyclerViewAdapter;
import org.article19.circulo.dialog.QuickStatusDialog;
import org.article19.circulo.model.Contact;
import org.article19.circulo.model.ContactStatusUpdate;
import org.article19.circulo.view.ContactAvatarView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import info.guardianproject.keanu.core.model.ContactList;
import info.guardianproject.keanu.core.model.ImErrorInfo;
import info.guardianproject.keanu.core.service.IChatSession;
import info.guardianproject.keanu.core.service.IChatSessionListener;
import info.guardianproject.keanu.core.service.IChatSessionManager;
import info.guardianproject.keanu.core.service.IContactList;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.ImServiceConstants;
import info.guardianproject.keanu.core.service.RemoteImService;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.accounts.AccountViewActivity;
import info.guardianproject.keanuapp.ui.camera.CameraActivity;
import info.guardianproject.keanuapp.ui.contacts.ContactsPickerActivity;
import info.guardianproject.keanuapp.ui.conversation.ConversationDetailActivity;

public class EditStatusActivity extends AppCompatActivity implements QuickStatusDialog.QuickStatusDialogListener, TagsRecyclerViewAdapter.TagsRecyclerViewAdapterListener {

    public static final String ARG_CONTACT_ID = "contact_id";

    private RecyclerView recyclerViewTags;
    private TagsRecyclerViewAdapter recyclerViewTagsAdapter;
    private TextView tvStatusHint;
    private List<String> listStatusTags;
    private List<String> listActionTags;
    private EditText editStatus;
    private Contact contact;
    private int selectedEmoji;
    private CheckBox cbUrgent;
    private TextView avatarViewEmoji;
    private View avatarViewEmojiLayout;
    private NoUnderlineSpan noUnderlineSpan;

    private FusedLocationProviderClient fusedLocationClient;
    private final static String GOOGLE_MAPS_BASE = "https://www.google.com/maps/search/?api=1&query=";

    private String mRoomId;
    private long mChatId;

    private ImApp mApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_status);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       mApp = (ImApp)getApplication();

       mRoomId = getIntent().getStringExtra("user");
        mChatId = getIntent().getLongExtra("id",-1L);

        int id = getIntent().getIntExtra(EditStatusActivity.ARG_CONTACT_ID, 0);
        contact = new Contact(0,mApp.getDefaultNickname(),mApp.getDefaultUsername());//CircleOf6Application.getInstance().getContactWithId(id);
        contact.setYou(true);

        setTitle(contact.getName());

        RecyclerView rvStatusUpdates = findViewById(R.id.rvStatusUpdates);
        if (contact.getStatus().canReply()) {
            rvStatusUpdates.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            StatusUpdatesRecyclerViewAdapter adapter = new StatusUpdatesRecyclerViewAdapter(this, contact);
            adapter.setUsingSeparateLayoutForFirstItem(false);
            rvStatusUpdates.setAdapter(adapter);
        } else {
            rvStatusUpdates.setVisibility(View.GONE);
        }

        ContactAvatarView avatarView = findViewById(R.id.avatarView);
        avatarView.setContact(contact);
        avatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCurrentAccount();
            }
        });

        avatarViewEmoji = findViewById(R.id.avatarViewEmoji);
        avatarViewEmojiLayout = findViewById(R.id.avatarViewEmojiLayout);
        avatarViewEmojiLayout.setVisibility(View.GONE);
        TextView tvName = findViewById(R.id.tvContactName);
        tvName.setText(contact.getName());

        tvStatusHint = findViewById(R.id.tvStatus);


        // We need a custom span to remove underlines from links.
        noUnderlineSpan = new NoUnderlineSpan();

        editStatus = findViewById(R.id.editStatus);
        editStatus.setAutoLinkMask(Linkify.ALL);
        editStatus.setLinkTextColor(ContextCompat.getColor(this, R.color.link_color));
        editStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Linkify.addLinks(s, Linkify.ALL);
                for (Object span : s.getSpans(0, s.length(), NoUnderlineSpan.class)) {
                    s.removeSpan(span);
                }
                for (Object span : s.getSpans(0, s.length(), ClickableSpan.class)) {
                    s.setSpan(new NoUnderlineSpan(), s.getSpanStart(span), s.getSpanEnd(span), s.getSpanFlags(span));
                }
                updateUIBasedOnStatusText();
            }
        });
        editStatus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                updateUIBasedOnStatusText();
            }
        });

        listActionTags = Arrays.asList(getResources().getStringArray(R.array.edit_status_action_tags));
        listStatusTags = Arrays.asList(getResources().getStringArray(R.array.edit_status_tags));

        cbUrgent = findViewById(R.id.cbUrgent);

        final View quickStatusButton = findViewById(R.id.btnQuickStatus);
        quickStatusButton.setOnClickListener(v -> QuickStatusDialog.showFromAnchor(quickStatusButton, EditStatusActivity.this));

        final View quickLocationButton = findViewById(R.id.btnQuickLocation);
        quickLocationButton.setOnClickListener(v -> {
            insertLocation();
        });

        recyclerViewTags = findViewById(R.id.rvTags);
        recyclerViewTags.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewTagsAdapter = new TagsRecyclerViewAdapter(this);
        recyclerViewTagsAdapter.setListener(this);
        recyclerViewTags.setAdapter(recyclerViewTagsAdapter);
        updateUIBasedOnStatusText();
    }

    private void insertLocation ()
    {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck == PackageManager.PERMISSION_DENIED)
        {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(editStatus, R.string.grant_perms, Snackbar.LENGTH_LONG).show();
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else {

            if (fusedLocationClient == null)
             fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object

                                if (editStatus.length() > 0)
                                    editStatus.append(" ");


                                StringBuffer geo = new StringBuffer();
                                geo.append(GOOGLE_MAPS_BASE);
                                geo.append(location.getLatitude());
                                geo.append(",");
                                geo.append(location.getLongitude());

                                editStatus.append(geo);

                            }
                        }
                    });



        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION)
        {
            insertLocation();
        }
    }

    private int MY_PERMISSIONS_REQUEST_LOCATION = 1001;

    @Override
    public void onQuickStatusSelected(int emoji) {
        selectedEmoji = emoji;
        if (selectedEmoji != 0) {
            StringBuffer sb = new StringBuffer();
            sb.append(Character.toChars(selectedEmoji));
            avatarViewEmoji.setText(sb);
            avatarViewEmojiLayout.setVisibility(View.VISIBLE);
        } else {
            avatarViewEmojiLayout.setVisibility(View.GONE);
        }
    }

    private void updateUIBasedOnStatusText() {
        if (editStatus.getText().length() == 0) {
            tvStatusHint.setVisibility(editStatus.hasFocus() ? View.GONE : View.VISIBLE);
            recyclerViewTagsAdapter.setTagBackgroundResourceId(R.drawable.status_tag_item_background_gray);
            recyclerViewTagsAdapter.setTags(listStatusTags);
        } else {
            tvStatusHint.setVisibility(View.GONE);
            recyclerViewTagsAdapter.setTagBackgroundResourceId(R.drawable.status_tag_item_background);
            recyclerViewTagsAdapter.setTags(listActionTags);
        }
    }

    @Override
    public void onTagClicked(String tag) {
        if (editStatus.getText().length() > 0 && !Character.isWhitespace(editStatus.getText().charAt(editStatus.getText().length() - 1))) {
            editStatus.append(" ");
        }
        editStatus.append(tag);
        updateUIBasedOnStatusText();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_status, menu);
        //menu.findItem(R.id.action_save).setEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_save:
                updateStatusAsync();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateStatusAsync ()
    {
        String newStatus = editStatus.getText().toString();

        new AsyncTask<String,Void,Boolean>()
        {

            @Override
            protected Boolean doInBackground(String... strings) {

                return  updateStatus(strings[0]);
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);


                if (success)
                {

                    finish();
                    Intent intent =new Intent(EditStatusActivity.this, ContactStatusActivity.class);
                    intent.putExtra("user",mRoomId);
                    intent.putExtra("id",mChatId);
                    startActivity(intent);

                }
                else
                {
                    //show error message?
                }
            }
        }.execute(newStatus);


    }


    private boolean updateStatus (String newStatus)
    {
        ContactStatusUpdate contactStatus = new ContactStatusUpdate();
        contactStatus.setSeen(true);
        contactStatus.setMessage(newStatus);
        contactStatus.setDate(new Date());
        contactStatus.setEmoji(selectedEmoji);
        contactStatus.setUrgent(cbUrgent.isChecked());

        Contact you = FragmentMain.getYouContact(this);
        you.getStatus().addUpdate(contactStatus);

        ImApp mApp = (ImApp)getApplication();
        IImConnection conn = RemoteImService.getConnection(mApp.getDefaultProviderId(),mApp.getDefaultAccountId());

        StringBuffer msg = new StringBuffer();

        if (!TextUtils.isEmpty(contactStatus.getMessage()))
            msg.append(contactStatus.getMessage());

        if (contactStatus.getEmoji() != -1)
            msg.append(' ').append(Character.toChars(selectedEmoji));

        if (contactStatus.isUrgent())
            msg.append(" #").append(getString(R.string.urgent));

        try {

            IChatSession session = conn.getChatSessionManager().getChatSession(mRoomId);
            if (session != null) {
                boolean isReply = false;
                boolean isEphemeral = false;
                boolean setLastmessage = false;
                session.sendMessage(msg.toString(), isReply, isEphemeral, setLastmessage, null);
                return true;
            }



        } catch (RemoteException e) {
            e.printStackTrace();


        }

        return false;

    }

    private void showCurrentAccount ()
    {
        Intent intent = new Intent(this, AccountViewActivity.class);
        intent.putExtra(ImServiceConstants.EXTRA_INTENT_PROVIDER_ID, mApp.getDefaultProviderId());
        intent.putExtra(ImServiceConstants.EXTRA_INTENT_ACCOUNT_ID, mApp.getDefaultAccountId());

        startActivity(intent);
    }

}
