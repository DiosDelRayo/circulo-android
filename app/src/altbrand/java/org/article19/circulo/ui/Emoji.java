package org.article19.circulo.ui;

/**
 * Created by N-Pex on 2018-11-08.
 */
public class Emoji {
    public static final int Safe = 0x1f60a;
    public static final int Unsure = 0x1f615;
    public static final int Scared = 0x1f628;
}
