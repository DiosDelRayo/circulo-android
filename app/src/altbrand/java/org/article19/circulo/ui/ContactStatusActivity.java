package org.article19.circulo.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.vanniktech.emoji.EmojiUtils;

import org.article19.circulo.FragmentMain;
import org.article19.circulo.dialog.QuickReplyDialog;
import org.article19.circulo.dialog.UpdateRemoveDialog;
import org.article19.circulo.model.Contact;
import org.article19.circulo.model.ContactStatus;
import org.article19.circulo.model.ContactStatusReply;
import org.article19.circulo.model.ContactStatusUpdate;
import org.article19.circulo.view.StatusViewHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import info.guardianproject.keanu.core.provider.Imps;
import info.guardianproject.keanu.core.ui.RoundedAvatarDrawable;
import info.guardianproject.keanu.core.util.DatabaseUtils;
import info.guardianproject.keanu.matrix.plugin.MatrixAddress;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.conversation.ConversationView;

import static info.guardianproject.keanu.core.KeanuConstants.DEFAULT_AVATAR_HEIGHT;
import static info.guardianproject.keanu.core.KeanuConstants.DEFAULT_AVATAR_WIDTH;
import static info.guardianproject.keanu.core.provider.Imps.ContactsColumns.SUBSCRIPTION_STATUS_NONE;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.ACCOUNT_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.AVATAR_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.CHAT_PROJECTION;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.NICKNAME_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.PRESENCE_STATUS_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.PROVIDER_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.SUBSCRIPTION_STATUS_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.SUBSCRIPTION_TYPE_COLUMN;
import static info.guardianproject.keanuapp.ui.conversation.ConversationView.USERNAME_COLUMN;

public class ContactStatusActivity extends AppCompatActivity implements StatusViewHolder.OnReplyListener {

    public static final String ARG_CONTACT_ID = "contact_id";
    private Contact contact;
    private StatusViewHolder viewHolder;
    private String mRemoteAddress = null;
    private long mChatId = -1;
    private LoaderManager mLoaderManager;
    private int LOADER_ID = 3001;
    private int mReplyLoaderId = 3002;

    private Uri mUri = null;
    private int loaderId = 100001;

    long mLastChatId=-1;
    String mRemoteNickname;
    RoundedAvatarDrawable mRemoteAvatar = null;
    Drawable mRemoteHeader = null;
    int mSubscriptionType = Imps.Contacts.SUBSCRIPTION_TYPE_NONE;
    int mSubscriptionStatus = SUBSCRIPTION_STATUS_NONE;

    long mProviderId = -1;
    long mAccountId = -1;
    long mInvitationId;
    private Activity mContext; // TODO
    private int mPresenceStatus;
    private Date mLastSeen;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_status);
   //     Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    //    setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRemoteAddress = getIntent().getStringExtra("user");
        mChatId = getIntent().getLongExtra("id",-1);

        //int id = getIntent().getIntExtra(ContactStatusActivity.ARG_CONTACT_ID, 0);

        contact = FragmentMain.getYouContact(this);// new Contact(1,"You","2125551212");// CircleOf6Application.getInstance().getContactWithId(id);

        setTitle(contact.getName());

        viewHolder = new StatusViewHolder(findViewById(R.id.statusRoot));
        viewHolder.populateWithContact(contact);
        viewHolder.setOnReplyListener(this);

        if (contact.isYou()) {
            viewHolder.etReply.setVisibility(View.GONE);
            viewHolder.quickReply.setVisibility(View.GONE);
            viewHolder.fabReply.setImageResource(R.drawable.ic_edit_white_32dp);
            viewHolder.fabReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UpdateRemoveDialog.showFromAnchor(v, new UpdateRemoveDialog.UpdateRemoveDialogListener() {
                        @Override
                        public void onRemoveSelected() {
                            contact.setStatus(null);
                           // CircleOf6Application.getInstance().statusUpdated(contact);
                            Toast.makeText(ContactStatusActivity.this, R.string.status_removed, Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void onUpdateSelected() {
                            Intent intent = new Intent(ContactStatusActivity.this, EditStatusActivity.class);
                            intent.putExtra(EditStatusActivity.ARG_CONTACT_ID, contact.getId());
                            intent.putExtra("user",mRemoteAddress);
                            startActivity(intent);
                        }
                    });
                }
            });
        }

        if (!contact.isYou()) {
            //CircleOf6Application.getInstance().setStatusSeen(contact);
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusUpdateReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean success = processIntent(getIntent());
        if (success) {
            LocalBroadcastManager.getInstance(this).registerReceiver(statusUpdateReceiver, new IntentFilter(Broadcasts.BROADCAST_STATUS_UPDATE_CHANGED));
            if (contact != null) {
                // Refresh
                viewHolder.refresh();
            }
        }
    }

    private BroadcastReceiver statusUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int contactId = intent.getIntExtra(Broadcasts.EXTRAS_CONTACT_ID, -1);
            if (contact != null && contactId == contact.getId()) {
                viewHolder.refresh();
            }
        }
    };

    private boolean processIntent(Intent intent)
    {

        if (intent != null) {
            mChatId = intent.getLongExtra("id", -1);

            if (mChatId != -1) {
                Uri contactUri = ContentUris.withAppendedId(Imps.Contacts.CONTENT_URI, mChatId);
                Cursor c = getContentResolver().query(contactUri, CHAT_PROJECTION, null, null, null);

                if (c == null)
                    return false;

                if (c.getColumnCount() < 8 || c.getCount() == 0)
                    return false;

                try {
                    boolean hasRow = c.moveToFirst();

                    if (!hasRow)
                        return false;
                } catch (IllegalStateException ise) {
                    return false;
                }
                try {
                    c.moveToFirst();
                } catch (IllegalStateException ise) {
                    return false;
                }

                if (c != null && (!c.isClosed()) && c.getCount() > 0) {
                    mProviderId = c.getLong(PROVIDER_COLUMN);
                    mAccountId = c.getLong(ACCOUNT_COLUMN);
                    mPresenceStatus = c.getInt(PRESENCE_STATUS_COLUMN);

                    mRemoteNickname = c.getString(NICKNAME_COLUMN);
                    mRemoteAddress = c.getString(USERNAME_COLUMN);

                    mSubscriptionType = c.getInt(SUBSCRIPTION_TYPE_COLUMN);

                    mSubscriptionStatus = c.getInt(SUBSCRIPTION_STATUS_COLUMN);

                }


                if (mRemoteAvatar == null) {
                    try {
                        mRemoteAvatar = DatabaseUtils.getAvatarFromCursor(c, AVATAR_COLUMN, DEFAULT_AVATAR_WIDTH, DEFAULT_AVATAR_HEIGHT);
                    } catch (Exception e) {
                    }

                    if (mRemoteAvatar == null) {
                        mRemoteAvatar = new RoundedAvatarDrawable(BitmapFactory.decodeResource(getResources(),
                                R.drawable.avatar_unknown));

                    }


                }

                if (mRemoteHeader == null) {
                    try {
                        mRemoteHeader = DatabaseUtils.getHeaderImageFromCursor(c, AVATAR_COLUMN, DEFAULT_AVATAR_WIDTH, DEFAULT_AVATAR_HEIGHT);
                    } catch (Exception e) {
                    }
                }


                c.close();

                startQuery(mChatId);

                return true;
            }

        }

        return false;
    }

    private synchronized void startQuery(long chatId) {

        mUri = Imps.Messages.getContentUriByThreadId(chatId);

        mLoaderManager = getSupportLoaderManager();

        if (mLoaderManager == null)
            mLoaderManager.initLoader(loaderId++, null, new MyLoaderCallbacks());
        else
            mLoaderManager.restartLoader(loaderId++, null, new MyLoaderCallbacks());

    }



    @Override
    public void onReply(Contact contact, String replyId, String message) {

    }

    @Override
    public void onQuickReply(Contact contact, String messageId,  View anchorButton) {
        QuickReplyDialog.showFromAnchor(viewHolder.itemView, anchorButton, new QuickReplyDialog.QuickReplyDialogListener() {
            @Override
            public void onQuickReplySelected(int emoji) {
                replyWithEmoji(emoji);
            }
        });
    }

    @Override
    public void onReply(Contact contact,  String messageId, int emoji) {

    }

    @Override
    public void onUnreply(Contact contact, int emoji) {

    }

    @Override
    public void handleInvite(Contact contact) {

    }

    private void replyWithEmoji(int emoji) {
        ContactStatusReply reply = new ContactStatusReply();
     //   reply.setContact(CircleOf6Application.getInstance().getYouContact());
        reply.setDate(new Date());
        reply.setType(ContactStatusReply.ReplyType.Emoji);
        reply.setEmoji(emoji);
     //   CircleOf6Application.getInstance().sendReply(contact, reply);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class MyLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        private int mLastCount = 0;

        public final String[] MESSAGE_PROJECTION = {
                Imps.Messages._ID,
                Imps.Messages.NICKNAME,
                Imps.Messages.BODY,
                Imps.Messages.TYPE,
                Imps.Messages.IS_DELIVERED,
                Imps.Messages.MIME_TYPE,
                Imps.Messages.THREAD_ID,
                Imps.Messages.REPLY_ID,
                Imps.Messages.DATE,
                Imps.Messages.PACKET_ID
        };

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            StringBuilder buf = new StringBuilder();

            /**
            StringBuilder buf = new StringBuilder();
            buf.append(Imps.Messages.THREAD_ID).append("=").append(mChatId);
            buf.append(" AND ");
            buf.append(Imps.Messages.REPLY_ID).append(" IS NULL ");
            buf.append(" AND (");
            buf.append(Imps.Messages.TYPE).append("=").append(Imps.MessageType.OUTGOING);
            buf.append(" OR ");
            buf.append(Imps.Messages.TYPE).append("=").append(Imps.MessageType.OUTGOING_ENCRYPTED);
            buf.append(") AND ");
            buf.append(Imps.Messages.BODY).append(" IS NOT NULL) GROUP BY (").append(Imps.Messages.NICKNAME); //GROUP BY
             **/

            CursorLoader loader = null;

            loader = new CursorLoader(ContactStatusActivity.this, mUri, MESSAGE_PROJECTION,
                    buf == null ? null : buf.toString(), null, Imps.Messages.DEFAULT_SORT_ORDER);

            return loader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor newCursor) {

            if (newCursor == null)
                return; // the app was quit or something while this was working
            else
            {
                newCursor.setNotificationUri(getApplicationContext().getContentResolver(), mUri);

                //mMessageAdapter.swapCursor(new ConversationView.DeltaCursor(newCursor));


                loadMessageData (newCursor);
            }

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {


        }
    }

    private void loadMessageData (Cursor cursor)
    {

        ContactStatus status = new ContactStatus();
        contact.setStatus(status);

        int colNickname = cursor.getColumnIndex(Imps.Messages.NICKNAME);
        int colBody = cursor.getColumnIndex(Imps.Messages.BODY);
        int colDate = cursor.getColumnIndex(Imps.Messages.DATE);
        int colType = cursor.getColumnIndex(Imps.Messages.TYPE);
        int colDelivered = cursor.getColumnIndex(Imps.Messages.IS_DELIVERED);
        int colPacketId = cursor.getColumnIndex(Imps.Messages.PACKET_ID);
        int colReplyId = cursor.getColumnIndex(Imps.Messages.REPLY_ID);

        while (cursor.moveToNext())
        {
            String msgUserAddress = cursor.getString(colNickname);
            String msgNickname = msgUserAddress;

            if (!TextUtils.isEmpty(msgUserAddress)) {

                if (msgNickname.startsWith("@"))
                    msgNickname = new MatrixAddress(msgNickname).getUser();
                else {
                    String[] parts = msgUserAddress.split("\\|");

                    if (parts.length > 1) {
                        msgNickname = parts[0];
                        msgUserAddress = parts[1];
                    }

                }
            }

            String msgBody = cursor.getString(colBody);

            if (!TextUtils.isEmpty(msgBody)) {

                msgBody = cleanBody (msgBody);

                Date msgDate = new Date(cursor.getLong(colDate));
                boolean isDelivered = cursor.getLong(colDelivered) > 0;
                int msgType = cursor.getInt(colType);
                String replyId = cursor.getString(colReplyId);
                String packetId = cursor.getString(colPacketId);

                if (!TextUtils.isEmpty(replyId)) {
                    List<ContactStatusUpdate> updates = contact.getStatus().getUpdates();

                    List<ContactStatusReply> replyList = status.getReplyList();
                    if (replyList == null)
                        replyList = new ArrayList<ContactStatusReply>();

                    for (ContactStatusUpdate update : updates) {
                        if (update.getMessageId().equals(replyId)) {

                            ContactStatusReply reply = new ContactStatusReply();
                            Contact contact = new Contact(mChatId, msgNickname, msgUserAddress);
                            ContactStatusReply csr = new ContactStatusReply();

                            if (EmojiUtils.isOnlyEmojis(msgBody)) {
                                csr.setEmoji(Character.codePointAt(msgBody, 0));
                                csr.setType(ContactStatusReply.ReplyType.Emoji);

                            } else if (!TextUtils.isEmpty(msgBody.trim())) {
                                csr.setMessage(msgBody);
                                csr.setType(ContactStatusReply.ReplyType.Message);

                            }

                            csr.setContact(contact);
                            csr.setDate(msgDate);
                            replyList.add(csr);
                            break;
                        }
                    }
                } else if (msgType == Imps.MessageType.OUTGOING || msgType == Imps.MessageType.OUTGOING_ENCRYPTED || msgType == Imps.MessageType.QUEUED) {

                    ContactStatusUpdate update = new ContactStatusUpdate();
                    update.setMessage(msgBody);
                    update.setSeen(false);
                    update.setDate(msgDate);
                    update.setUrgent(msgBody.contains("#" + getString(R.string.urgent)));
                    update.setMessageId(packetId);

                    if (isDelivered)
                        update.setDeliveryStatus(ContactStatusUpdate.STATUS_DELIVERED);
                    else if (msgType == Imps.MessageType.QUEUED)
                        update.setDeliveryStatus(ContactStatusUpdate.STATUS_QUEUED);
                    else
                        update.setDeliveryStatus(ContactStatusUpdate.STATUS_SENT);

                    status.addUpdate(update);
                    contact.setStatus(status);


                } else if (msgType == Imps.MessageType.INCOMING || msgType == Imps.MessageType.INCOMING_ENCRYPTED) {

                }
            }

        }

        viewHolder.populateWithContact(contact);
    }

    private String cleanBody (String body)
    {
        StringBuffer sb = new StringBuffer();

        String lines[] = body.split("\\r?\\n");

        for (String line : lines)
        {
            if (!line.startsWith(">"))
                if (line.trim().length() > 0)
                sb.append(line.trim()).append("\n");
        }

        return sb.toString();
    }
}
