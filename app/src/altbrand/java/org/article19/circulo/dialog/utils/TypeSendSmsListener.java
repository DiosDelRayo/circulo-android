package org.article19.circulo.dialog.utils;

/**
 * Created by Edgar Salvador Maurilio on 22/11/2015.
 */
public interface TypeSendSmsListener {

    void sendSmsComeAndGetMe();

    void sendSmsCallMeNeed();

    void sendSmsNeedToTalk();

    void sendSmsIamOk();

}
