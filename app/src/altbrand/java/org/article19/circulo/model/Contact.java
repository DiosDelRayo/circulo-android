package org.article19.circulo.model;

import android.text.TextUtils;

public class Contact {

    private long id;
    private String name;
    private String address;
    private ContactStatus status;
    private boolean you;
    private int position;

    public Contact(long id, String name, String address) {

        this.id = id;
        this.name = name;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public ContactStatus getStatus() {
        if (status == null) {
            status = new ContactStatus();
        }
        return status;
    }

    public void setStatus(ContactStatus status) {
        this.status = status;
    }

    public  boolean isEmpty () {
        return TextUtils.isEmpty(address);
    }

    public boolean isYou() {
        return you;
    }

    public void setYou(boolean you) {
        this.you = you;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
